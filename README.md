# Python | Tutorials

- Flask Python environment
- Not running on Apache and should not be used for anything else but tests to learn python. 

**Edit the hosts:**

    $ sudo vim /etc/hosts

**Then add the line :**

    127.0.0.1 flask.kiwipal-local.com

**Build image**

    $ docker-compose build --no-cache

**Run container**
    $ docker-compose up -d

**Stop container**

    $ docker-compose down

**If needed, access the container bash**

    $ docker exec -it python_web_flask /bin/bash

**Website available at:**

    http://flask.kiwipal-local.com:8080