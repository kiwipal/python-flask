from flask import Flask,render_template
import socket

app = Flask(__name__)
app.config['SERVER_NAME'] = 'flask.kiwipal-local.com:8080'

@app.route("/")
def index():
    try:
        return render_template('index.html', message="Hello World!")
    except:
        return render_template('error.html')

@app.route("/test/")
def test():
    try:

        total = [
            "Lundi",
            "Mardi"
        ]

        if 1==1:
            return render_template('test.html', total=total)
        else:
            return "NOK"
        
    except:
        return render_template('error.html')

if __name__ == "__main__":
    app.run(debug = True, host='0.0.0.0', port=8080)

